#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 01:45:30 2020

@author: heba
"""

######################################
""" Importing Necessary Libraries """
######################################
from tkinter import *
from PIL import ImageTk, Image
import sqlite3
import os
import pyttsx3

class Count2Master():
    ###########################
    """ StopWatch Backend """
    ##########################
    def start(self):
        if (self.Entr.get() == '') or (self.Entr.get() == 'ex: SIFT, NLG, Data Engineeing Track...'):
            messagebox.showerror('ERROR!', 'Enter Your Desired Job Title To Proceed!')
        else:
            result = self.Retrieve(str(self.Entr.get()))
            if result is not None:
                self.timer.set(str(result))
            self.Flag = 0
            self.main_timer()
                
    def reset(self):
        self.Flag = 1
        self.timer.set("00:00:00")
        self.btn_txt.set('START')
    
    def stop(self):
        self.Flag = 1
        self.btn_txt.set('Resume')
    
    def Congrats(self):
        if int(self.h) < 10:
            self.engine.say("YOU MUST TAKE THE EXCEPERIENCE TILL THE END\n COMPLETE MINIMUM 10 HOURS")
            self.engine.say("Don't Give Up NOW!")
            self.engine.runAndWait()
        else:
            self.engine.say("CONGRATULATIONS, YOU 're NOW UP ONE MORE Degree")
            self.engine.say("Keep Going Dear")
            self.engine.runAndWait()
        
    def main_timer(self):
        if self.Flag == 0:
            self.time = str(self.timer.get())
            self.h, self.m, self.s = map(int, self.time.split(":"))
            h = int(self.h)
            m = int(self.m)
            s = int(self.s)
            if(s<59):
                s+=1
            elif(s==59):
                s=0
                if(m<59):
                    m+=1
                elif(m==59):
                    h+=1
            if(h<10):
                h = str(0)+str(h)
            else:
                h= str(h)
            if(m<10):
                m = str(0)+str(m)
            else:
                m = str(m)
            if(s<10):
                s=str(0)+str(s)
            else:
                s=str(s)
                
            if self.timer.get() == "10:00:00":
                duration = 1  
                freq = 440  
                os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
                self.engine.say("WooW, You did Greate, You Reached The Half of The Way")
                self.engine.say("Keep Learning to Reach The Master of %s, It seams Pretty Interesting" %str(self.Entr.get()))
                self.engine.runAndWait() 
                
            elif self.timer.get() == "20:00:00":
                duration = 1  
                freq = 440  
                os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
                self.engine.say("Congratulations, You Have Just Reached Twenty Hours Learning %s, If You still Don't Complete Press Continue" %str(self.Entr.get()))
                self.engine.runAndWait() 
                self.Flag =1
                
            self.time=h+":"+m+":"+s
            self.timer.set(self.time)
            Query = "UPDATE PROGRESS SET Last_Progress = ? where Topic = ?" 
            self.DB_CONN.execute(Query, (self.time, self.Entr.get()))
            self.DB_CONN.commit()
            if(self.Flag==0):
                self.Home.after(930,self.main_timer)
    ##########################
    """ Variables' States """
    #########################
    def DB(self):
        conn = None
        Query = "CREATE TABLE IF NOT EXISTS PROGRESS(ID integer PRIMARY KEY AUTOINCREMENT, Topic VARCHAR(200), Last_Progress VARCHAR(10));"
        try:
            conn = sqlite3.connect("MY APP.db")
            conn.execute(Query)
        except Error as e:
            print(e)
        conn.commit()
        return conn
    
    
    def Retrieve(self, topic):
        Query = "SELECT * FROM PROGRESS;"
        CURSOR = self.DB_CONN.execute(Query)
        for row in CURSOR:
            if topic == row[1]:
                Progress = row[2]
                return Progress
        Query2 = "INSERT INTO PROGRESS(Topic, Last_Progress) VALUES(?,'00:00:00');" 
        self.DB_CONN.execute(Query2, (topic,))
        self.DB_CONN.commit()
    
    ####################################
    """ GUI Implementaion Functions """
    ###################################
    def Set_Background(self, Panel, Img_path):
        canvas = Canvas(Panel,bg = "#E5D9CC", width=self.WIDTH, height=self.HEIGTH)
        canvas.pack(fill='both')
        return canvas    
    
    def on_entry_click(self,event):
        """function that gets called whenever entry is clicked"""
        if self.Entr.get() == 'ex: SIFT, NLG, Data Engineeing Track...':
           self.Entr.delete(0, "end")
           self.Entr.insert(0, '')
           self.Entr.config(fg = 'black')
           
    def on_focusout(self,event):
        if self.Entr.get() == '':
            self.Entr.insert(0, 'ex: SIFT, NLG, Data Engineeing Track...')
            self.Entr.config(fg = 'grey')
    
    def __init__(self):
        self.Flag = 0
        self.DB_CONN = self.DB()
        self.h = 0
        self.engine = pyttsx3.init()
        voices = self.engine.getProperty('voices')
        self.engine.setProperty('voice', voices[0].id) 
        self.Home = Tk()
        self.Home.resizable(False,False)
        self.Home.title("Master Skills In 20hrs!")
        self.WIDTH, self.HEIGTH = 600, 400
        self.Home.geometry('{}x{}+750+350'.format(self.WIDTH, self.HEIGTH))
        Img_path = 'output-onlinejpgtools.jpg'
        Canvas = self.Set_Background(self.Home, Img_path)
        self.timer = StringVar()
        self.timer.set("00:00:00")
        self.Text1 = Canvas.create_text((300,30), font=("Times New Roman", 18, "italic", "bold"), fill ="#063630", text="Keep Learning New Technologies!")
        self.Text2 = Canvas.create_text((300,80), font=("Times New Roman", 20, "italic", "bold"), fill ="#063630", text="Enter New Topic")
        self.str = StringVar()
        large_font = ('Verdana',15)
        self.Entr = Entry(self.Home, textvariable=self.str, width=40, relief='flat', borderwidth=5, font=large_font, highlightbackground="#E5D9CC",highlightcolor = "#E5D9CC", highlightthickness=2 )
        self.Entr.insert(0, 'ex: SIFT, NLG, Data Engineeing Track...')
        self.Entr.bind('<FocusIn>', self.on_entry_click)
        self.Entr.bind('<FocusOut>', self.on_focusout)
        self.Entr.config(fg = 'grey')
        Entr = Canvas.create_window(300, 150, anchor=CENTER, window=self.Entr)
        self.lb = Label(self.Home,textvariable=self.timer, relief='flat', fg="#DD845F", highlightbackground="#E5D9CC",highlightcolor = "#E5D9CC", highlightthickness=2)
        self.lb.config(font=("Courier", 40, "italic", "bold"))
        self.Text3 = Canvas.create_window((300,230), window=self.lb)
        self.btn_txt = StringVar()
        self.btn_txt.set('START')
        btn1 = Button(self.Home , textvariable=self.btn_txt, borderwidth=2, width = 4, height =1, font=("Times New Roman", 18), fg="#DD845F", highlightbackground="light cyan",highlightcolor = "cyan", highlightthickness=2 ,bg='snow', command=self.start)
        butn1 = Canvas.create_window(50, 330, anchor=SW, window=btn1)               
        btn2 = Button(self.Home , text='RESET' , borderwidth=2, width = 3, height =1, font=("Times New Roman", 18), fg="#DD845F", highlightbackground="light cyan",highlightcolor = "cyan", highlightthickness=2 ,bg='snow', command=self.reset)
        butn2 = Canvas.create_window(200, 330, anchor=SW, window=btn2)               
        btn3=Button(self.Home , text='STOP' , borderwidth=2, width = 3, height =1, font=("Times New Roman", 18), fg="#DD845F", highlightbackground="light cyan",highlightcolor = "cyan", highlightthickness=2 ,bg='snow', command=self.stop)
        butn3 = Canvas.create_window(350, 330, anchor=SW, window=btn3)      
        btn3=Button(self.Home , text='Done' , borderwidth=2, width = 3, height =1, font=("Times New Roman", 18), fg="#DD845F", highlightbackground="light cyan",highlightcolor = "cyan", highlightthickness=2 ,bg='snow', command=self.Congrats)
        butn3 = Canvas.create_window(500, 330, anchor=SW, window=btn3)
        self.Home.mainloop()
        
Count2Master()
